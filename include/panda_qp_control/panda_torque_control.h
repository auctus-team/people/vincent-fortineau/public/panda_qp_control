// Copyright (c) 2017 Franka Emika GmbH
// Use of this source code is governed by the Apache-2.0 license, see LICENSE

#pragma once

#include "pinocchio/fwd.hpp"
#include <memory>
#include <string>
#include <vector>
#include <iostream>
#include <fstream>

#include <controller_interface/multi_interface_controller.h>
#include <hardware_interface/joint_command_interface.h>
#include <hardware_interface/robot_hw.h>
#include <ros/param.h>
#include <ros/node_handle.h>
#include <ros/package.h>
#include <ros/time.h>

#include <unsupported/Eigen/MatrixFunctions> // for matrix square root

#include <tf/transform_listener.h> // for tf servoing
#include <tf_conversions/tf_eigen.h> // for tf servoing
#include <eigen_conversions/eigen_msg.h> // for topic servoing

#include <franka_hw/franka_model_interface.h>
#include <franka_hw/franka_state_interface.h>

#include <franka/robot.h>
#include "franka/robot_state.h"

#include <torque_qp/controller.h>
#include <moveit_trajectory_interface/moveit_trajectory_interface.hpp>

#include <dynamic_reconfigure/server.h>
#include <panda_qp_control/TorqueQPCtrlConfig.h>

#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/AccelStamped.h>
#include <geometry_msgs/Accel.h>
#include <geometry_msgs/WrenchStamped.h>
#include <std_msgs/Bool.h>
#include <std_msgs/Float64MultiArray.h>

// servoing type:
#define MOVE_IT 1
#define TF 2
#define TOPIC 3

#define SERVOING_TYPE TOPIC

namespace panda_qp_control {

class TorqueController : public controller_interface::MultiInterfaceController<
                                                franka_hw::FrankaModelInterface,
                                                // hardware_interface::VelocityJointInterface,
                                                hardware_interface::EffortJointInterface,
                                                franka_hw::FrankaStateInterface> {  
                                                                                                    
public:
    /**
    * @brief Franka Panda initialization routine
    */
    bool init(hardware_interface::RobotHW* robot_hw, ros::NodeHandle& node_handle) override;
    
    /**
    * @brief Franka Panda starting routine
    */
    void starting(const ros::Time&) override;
    
    /**
    * @brief Franka Panda controller update routine
    */
    void update(const ros::Time&, const ros::Duration& period) override;
    
    void publishCartesianState();

    void publishAdaptiveGains(Eigen::MatrixXd adaptive_gain_matrix); 

    template <class T>
    /**
        * \fn bool getRosParam
        * \brief Load a rosparam to a variable
        * \param const std::string& param_name the name of the ros param
        * \param T& param_data the variable linked to the ros param
        * \return true if the ros param exist and can be linked to the variable
        */
    bool getRosParam(const std::string& param_name, T& param_data, bool verbose = false)
    {
        if (!ros::param::get(param_name, param_data))
        {
            ROS_FATAL_STREAM(" Problem loading parameters " << param_name << ". Check the Yaml config file. Killing ros");
            ros::shutdown();
        }
        else
        {
        if (verbose)
            ROS_INFO_STREAM(param_name << " : " << param_data);
        }
        return true;
    }

    /**
        * \fn bool getRosParam
        * \brief Load a rosparam to a variable
        * \param const std::string& param_name the name of the ros param
        * \param Eigen::VectorXd& param_data a Eigen vector linked to the std::vector ros param
        * \return true if the ros param exist and can be linked to the variable
        */
    bool getRosParam(const std::string& param_name, Eigen::VectorXd& param_data, bool verbose = false)
    {
        std::vector<double> std_param;
        if (!ros::param::get(param_name, std_param))
        {
            ROS_FATAL_STREAM(" Problem loading parameters " << param_name << ". Check the Yaml config file. Killing ros");
            ros::shutdown();
        }
        else
        {
        if (std_param.size() == param_data.size())
        {
            for (int i = 0; i < param_data.size(); ++i)
            param_data(i) = std_param[i];
            if (verbose)
            ROS_INFO_STREAM(param_name << " : " << param_data.transpose());
            return true;
        }
        else
        {
            ROS_FATAL_STREAM("Wrong matrix size for param " << param_name << ". Check the Yaml config file. Killing ros");
            ros::shutdown();
        }
        }
    }
private:
    std::string arm_id;

    realtime_tools::RealtimePublisher<geometry_msgs::PoseStamped> pose_des_publisher;
    realtime_tools::RealtimePublisher<geometry_msgs::PoseStamped> pose_curr_publisher;
    realtime_tools::RealtimePublisher<geometry_msgs::TwistStamped> vel_curr_publisher;
    realtime_tools::RealtimePublisher<geometry_msgs::AccelStamped> acc_curr_publisher;
    realtime_tools::RealtimePublisher<std_msgs::Float64MultiArray> adapt_gain_publisher;
    geometry_msgs::Accel cartesian_acceleration_msg;
    geometry_msgs::Twist cartesian_velocity_msg;
    geometry_msgs::Pose cartesian_pose_msg;
    geometry_msgs::Pose cartesian_des_pose_msg;
    std_msgs::Float64MultiArray adapt_gain_msg;

    QPController::Torque torque_qp;
    ros::NodeHandle node_handle;
    ros::NodeHandle parent_node_handle;
    std::unique_ptr<franka_hw::FrankaStateHandle> state_handle_;
    std::unique_ptr<franka_hw::FrankaModelHandle> model_handle_;

    hardware_interface::VelocityJointInterface* velocity_joint_interface_;
    std::vector<hardware_interface::JointHandle> velocity_joint_handles_;  
    std::vector<hardware_interface::JointHandle> joint_handles_;
    bool initialized = false;
    bool reset_target_pose = false;

    // Trajectory variables
    #if SERVOING_TYPE == MOVE_IT
        TrajectoryInterface trajectory;
    #elif SERVOING_TYPE == TF
        tf::TransformListener marker_listener;
        tf::StampedTransform marker_tf;
    #elif SERVOING_TYPE == TOPIC
        ros::Subscriber EE_sub;
        geometry_msgs::Pose ee_pose;
    #endif
    ros::Subscriber f_ext_sub;
    ros::Subscriber ctrl_mode_sub;
    ros::Subscriber joint_state_sec_sub; // for the state of a second robot if it exists

    std::string reference_frame_name;
    std::string marker_tf_frame_name; 
    std::string control_frame_name; // for moveit control

    // Pinocchio Model and Data
    pinocchio::Model model;
    pinocchio::Data data;
    pinocchio::Motion xd;
    Eigen::Matrix<double,6,1> xdd_star; // main task acceleration provided to torque qp solver

    Eigen::VectorXd p_gains;
    Eigen::MatrixXd p_gains_m;
    Eigen::VectorXd d_gains;
    Eigen::MatrixXd d_gains_m;
    Eigen::VectorXd kp_reg;
    Eigen::VectorXd kp_reg_q;
    Eigen::VectorXd ki_reg_q;
    Eigen::VectorXd dtorque_max;
    bool adapt_gains; // to adapt gains to another panda robot configuration
    Eigen::Matrix<double,7,1> q2; // joint configuration of the other panda
    Eigen::Matrix<double,7,1> dq2; // joint configuration of the other panda
    Eigen::VectorXd delta_p_gains; // joint variation of the other panda
    
    // Offset for the EE control servoing
    Eigen::Matrix4d ee_tf;
    // Jacobian matrix
    Eigen::MatrixXd J;
    Eigen::MatrixXd J2;
    Eigen::MatrixXd lambda2; // apparent mass matrix of the second robot
    // External (unmodeled) perturbation force, at the robot end effector expressed in the robot base
    Eigen::Matrix<double,6,1> f_ext;
    bool joint_ctrl_mode;

    // dynamic reconfigure 
    void reconfigureCallback(TorqueQPCtrlConfig &config, uint32_t level); 
    boost::shared_ptr<dynamic_reconfigure::Server<TorqueQPCtrlConfig>> server;
    boost::recursive_mutex reconf_mutex_;

    // topic subscriber callback for endpoint pose
    void endeffectorPoseCallback(const geometry_msgs::PoseStamped::ConstPtr& );
    // topic subscriber callback to set f_ext
    void setFextCallback(const geometry_msgs::WrenchStamped::ConstPtr&);
    // topic subscriber callback to get control mode
    void getCtrlModeCallback(const std_msgs::Bool::ConstPtr&);
    // topic subscriber callback to get the second robot joint data
    void getJointStateSecCallback(const sensor_msgs::JointState::ConstPtr&);

};

}  // namespace panda_qp_control

namespace tf {
    void accelEigenToMsg(const Eigen::Matrix<double,6,1> &e, geometry_msgs::Accel &m)
    {
        m.linear.x = e[0];
        m.linear.y = e[1];
        m.linear.z = e[2];
        m.angular.x = e[3];
        m.angular.y = e[4];
        m.angular.z = e[5];
    }
} // namespace tf

