// Copyright (c) 2017 Franka Emika GmbH
// Use of this source code is governed by the Apache-2.0 license, see LICENSE

#pragma once

#include "pinocchio/fwd.hpp"
#include <memory>
#include <string>
#include <vector>
#include <iostream>
#include <fstream>

#include <controller_interface/multi_interface_controller.h>
#include <hardware_interface/joint_command_interface.h>
#include <hardware_interface/robot_hw.h>
#include <ros/node_handle.h>
#include <ros/time.h>

#include <franka_hw/franka_model_interface.h>
#include <franka_hw/franka_state_interface.h>

#include <franka/robot.h>
#include "franka/robot_state.h"

#include <velocity_qp/controller.h>
#include <moveit_trajectory_interface/moveit_trajectory_interface.hpp>




namespace panda_qp_control {

class VelocityController : public controller_interface::MultiInterfaceController<
                                                franka_hw::FrankaModelInterface,
                                                hardware_interface::VelocityJointInterface,
                                                hardware_interface::EffortJointInterface,
                                                franka_hw::FrankaStateInterface> {  
                                                                                                    
public:
    /**
    * @brief Franka Panda initialization routine
    */
    bool init(hardware_interface::RobotHW* robot_hw, ros::NodeHandle& node_handle) override;
    
    /**
    * @brief Franka Panda starting routine
    */
    void starting(const ros::Time&) override;
    
    /**
    * @brief Franka Panda controller update routine
    */
    void update(const ros::Time&, const ros::Duration& period) override;
    
    void publishCartesianState();

    template <class T>
    /**
        * \fn bool getRosParam
        * \brief Load a rosparam to a variable
        * \param const std::string& param_name the name of the ros param
        * \param T& param_data the variable linked to the ros param
        * \return true if the ros param exist and can be linked to the variable
        */
    bool getRosParam(const std::string& param_name, T& param_data, bool verbose = false)
    {
        if (!ros::param::get(param_name, param_data))
        {
            ROS_FATAL_STREAM(" Problem loading parameters " << param_name << ". Check the Yaml config file. Killing ros");
            ros::shutdown();
        }
        else
        {
        if (verbose)
            ROS_INFO_STREAM(param_name << " : " << param_data);
        }
        return true;
    }

    /**
        * \fn bool getRosParam
        * \brief Load a rosparam to a variable
        * \param const std::string& param_name the name of the ros param
        * \param Eigen::VectorXd& param_data a Eigen vector linked to the std::vector ros param
        * \return true if the ros param exist and can be linked to the variable
        */
    bool getRosParam(const std::string& param_name, Eigen::VectorXd& param_data, bool verbose = false)
    {
        std::vector<double> std_param;
        if (!ros::param::get(param_name, std_param))
        {
            ROS_FATAL_STREAM(" Problem loading parameters " << param_name << ". Check the Yaml config file. Killing ros");
            ros::shutdown();
        }
        else
        {
        if (std_param.size() == param_data.size())
        {
            for (int i = 0; i < param_data.size(); ++i)
            param_data(i) = std_param[i];
            if (verbose)
            ROS_INFO_STREAM(param_name << " : " << param_data.transpose());
            return true;
        }
        else
        {
            ROS_FATAL_STREAM("Wrong matrix size for param " << param_name << ". Check the Yaml config file. Killing ros");
            ros::shutdown();
        }
        }
    }

private:
    
    realtime_tools::RealtimePublisher<geometry_msgs::PoseStamped> pose_curr_publisher;
    realtime_tools::RealtimePublisher<geometry_msgs::TwistStamped> vel_curr_publisher;
    geometry_msgs::Twist cartesian_velocity_msg;
    geometry_msgs::Pose cartesian_pose_msg;

    QPController::Velocity velocity_qp;
    ros::NodeHandle node_handle;
    std::unique_ptr<franka_hw::FrankaStateHandle> state_handle_;
    std::unique_ptr<franka_hw::FrankaModelHandle> model_handle_;

    hardware_interface::VelocityJointInterface* velocity_joint_interface_;
    std::vector<hardware_interface::JointHandle> velocity_joint_handles_;  
    std::vector<hardware_interface::JointHandle> joint_handles_;
    std::string control_level;
    bool initialized = false;

    // Trajectory variables
    TrajectoryInterface trajectory;
    std::string controlled_frame;
    Eigen::VectorXd p_gains;
    // Pinocchio Model and Data
    pinocchio::Model model;
    pinocchio::Data data;
    pinocchio::Motion xd;
};

}  // namespace panda_qp_control

