// Copyright (c) 2017 Franka Emika GmbH0
// Use of this source code is governed by the Apache-2.0 license, see LICENSE
#include <panda_qp_control/panda_velocity_control.h>
#include <cmath>
#include <memory>
#include <chrono> 
#include <controller_interface/controller_base.h>
#include <franka/robot_state.h>
#include <pluginlib/class_list_macros.h>
#include <ros/ros.h>

using namespace std;

namespace panda_qp_control{


bool VelocityController::init(hardware_interface::RobotHW* robot_hardware, ros::NodeHandle& node_handle) {    
    this->node_handle = node_handle;
    //--------------------------------------
    // LOAD ROBOT
    //--------------------------------------
    string arm_id;
    if (!node_handle.getParam("arm_id", arm_id)) {
        ROS_ERROR_STREAM("Could not read parameter arm_id");
        return false;
    }   
    
    std::vector<std::string> joint_names;
    if (!node_handle.getParam("joint_names", joint_names)) {
        ROS_ERROR("Could not parse joint names");
    }
    if (joint_names.size() != 7) {
        ROS_ERROR_STREAM("Wrong number of joint names, got "
        << joint_names.size() << " instead of 7 names!");
        return false;
    }
   
    auto state_interface = robot_hardware->get<franka_hw::FrankaStateInterface>();
    if (state_interface == nullptr) {
        ROS_ERROR("Could not get state interface from hardware");
        return false;
    }
    try {
        state_handle_ = std::make_unique<franka_hw::FrankaStateHandle>( state_interface->getHandle("panda_robot"));
    } catch (const hardware_interface::HardwareInterfaceException& e) {
        ROS_ERROR_STREAM(
            "Exception getting state handle: " << e.what());
            return false;
    }

   velocity_joint_interface_ = robot_hardware->get<hardware_interface::VelocityJointInterface>();
    if (velocity_joint_interface_ == nullptr) {
        ROS_ERROR(
            "JointVelocityExampleController: Error getting velocity joint interface from hardware!");
            return false;
    }
    velocity_joint_handles_.resize(7);
    for (size_t i = 0; i < 7; ++i) {
        try {
            velocity_joint_handles_[i] = velocity_joint_interface_->getHandle(joint_names[i]);
        } catch (const hardware_interface::HardwareInterfaceException& ex) {
            ROS_ERROR_STREAM(
            "JointVelocityExampleController: Exception getting joint handles: " << ex.what());
            return false;
        }
    }
    
    pose_curr_publisher.init(node_handle, "current_cartesian_pose", 1);
    vel_curr_publisher.init(node_handle, "current_cartesian_velocity", 1);
    return true;
}

void VelocityController::starting(const ros::Time&)
{
    std::string robot_description;
    getRosParam("/panda/robot_description", robot_description);
    std::vector<std::string> joint_names;
    ros::param::get("/panda/velocity_control/joint_names", joint_names);
    p_gains.resize(6);
    getRosParam("/panda/velocity_control/p_gains", p_gains);
    franka::RobotState robot_state = state_handle_->getRobotState(); 
    //Get robot current state
    Eigen::Matrix<double,7,1>  q_init(robot_state.q.data());

    if (!initialized) // franka_control starts twice but the first one doesn't initialize the velocity_qp ...
    {
        velocity_qp.setRegularisationType(QPController::Velocity::RegularisationType::TrackConfiguration);
        initialized = velocity_qp.init(robot_description,joint_names) ;
        trajectory.interface->init(this->node_handle,"/panda/robot_description",q_init,joint_names);
        model = velocity_qp.getRobotModel();
        data = pinocchio::Data(model);
    }
}

void VelocityController::update(const ros::Time&, const ros::Duration& period) {    

    //--------------------------------------
    // ROBOT STATE
    //--------------------------------------
    // get state variables
    franka::RobotState robot_state = state_handle_->getRobotState(); 
    //Get robot current state
    Eigen::Matrix<double,7,1>  q(robot_state.q.data());
    Eigen::Matrix<double,7,1>  qd(robot_state.dq.data());
    
    // Update model
      // First calls the forwardKinematics on the model, then computes the placement of each frame.
    pinocchio::forwardKinematics(model,data,q,qd);
    pinocchio::updateFramePlacements(model,data);

    //Update the trajectory
    trajectory.interface->updateTrajectory();
    pinocchio::SE3 oMdes(trajectory.interface->getCartesianPose().matrix());
    // Compute error
    const pinocchio::SE3 tipMdes = data.oMf[model.getFrameId(velocity_qp.getControlledFrame())].actInv(oMdes);
    Eigen::Matrix<double,6,6> adj = data.oMf[model.getFrameId(velocity_qp.getControlledFrame())].toActionMatrix();
    Eigen::Matrix<double,6,1> err = adj *pinocchio::log6(tipMdes).toVector();
    // Proportional controller with feedforward
    Eigen::Matrix<double,6,1> xd_star = p_gains.cwiseProduct(err) + trajectory.interface->getCartesianVelocity();
    velocity_qp.setRegularizationConfiguration(trajectory.interface->getJointConfiguration());

    Eigen::Matrix<double,7,1>  joint_command = velocity_qp.update(q,qd,xd_star);

    for (size_t i = 0; i < 7; ++i)
    {
        velocity_joint_handles_[i].setCommand(joint_command(i));
    }      
    publishCartesianState();
}

void VelocityController::publishCartesianState()
{
    Eigen::Affine3d cartesian_pose;
    cartesian_pose.matrix() = data.oMf[model.getFrameId(velocity_qp.getControlledFrame())].toHomogeneousMatrix();
    tf::poseEigenToMsg(cartesian_pose,cartesian_pose_msg);
    xd = pinocchio::getFrameVelocity(model, data, model.getFrameId(velocity_qp.getControlledFrame()),pinocchio::ReferenceFrame::WORLD );
    tf::twistEigenToMsg(xd.toVector(), cartesian_velocity_msg);
    //Publish robot desired pose
    if (pose_curr_publisher.trylock())
    {
        pose_curr_publisher.msg_.header.stamp = ros::Time::now();
        pose_curr_publisher.msg_.header.frame_id = "world";
        pose_curr_publisher.msg_.pose = cartesian_pose_msg;
        pose_curr_publisher.unlockAndPublish();
    }
    //Publish robot desired velocity
    if (vel_curr_publisher.trylock())
    {
        vel_curr_publisher.msg_.header.stamp = ros::Time::now();
        vel_curr_publisher.msg_.header.frame_id = "world";
        vel_curr_publisher.msg_.twist = cartesian_velocity_msg;
        vel_curr_publisher.unlockAndPublish();
    }
}

}  // namespace panda_qp_control

PLUGINLIB_EXPORT_CLASS(panda_qp_control::VelocityController,
controller_interface::ControllerBase)
