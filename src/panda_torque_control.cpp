// Copyright (c) 2017 Franka Emika GmbH0
// Use of this source code is governed by the Apache-2.0 license, see LICENSE
#include <panda_qp_control/panda_torque_control.h>
#include <cmath>
#include <memory>
#include <chrono> 
#include <controller_interface/controller_base.h>
#include <franka/robot_state.h>
#include <pluginlib/class_list_macros.h>
#include <ros/ros.h>

using namespace std;

namespace panda_qp_control{

void TorqueController::reconfigureCallback(TorqueQPCtrlConfig &config, uint32_t level)
{
    // ROS_INFO_STREAM_THROTTLE(1.0, "New p gain:" << endl << 
    //     "\t position: " << config.Kp_x <<", " << config.Kp_y <<", " << config.Kp_z << endl <<  
    //     "\t orientation: "<< config.Kp_rx <<", " << config.Kp_ry <<", " << config.Kp_rz);

    // p_gains[0] = config.Kp_x;
    // p_gains[1] = config.Kp_y;
    // p_gains[2] = config.Kp_z;
    // ROS_INFO_STREAM_THROTTLE(1, "call back");

    if (config.set_Kp)
    {
        for (int i=0;i<6;i++)
        {
            p_gains[i] = config.Kp;; // 2*sqrt(p_gains[i])
        }

        for (int i=0;i<6;i++)
        {
            d_gains[i] = 2*sqrt(p_gains[i]);
        }
        ROS_INFO_STREAM_THROTTLE(1.0, "New p gain matrix recieved.");
        ROS_INFO_STREAM_THROTTLE(1.0, p_gains);
    }

     if (config.adapt_Kp)
    {
        adapt_gains = true;
        for (int i=0;i<6;i++)
        {
            delta_p_gains[i] = config.delta_Kp2; // 2*sqrt(p_gains[i])
        }
        ROS_INFO_STREAM_THROTTLE(1.0, "Gains adapted.");
        ROS_INFO_STREAM_THROTTLE(1.0, delta_p_gains);
    }
    else
    {
        adapt_gains = false;
    }

    if (config.reset_target_pose)
    {
        reset_target_pose = true;
    }
    else
    {
        reset_target_pose = false;
    }

}

void TorqueController::endeffectorPoseCallback(const geometry_msgs::PoseStamped::ConstPtr& posestpd)
{
    #if SERVOING_TYPE == TOPIC
        this->ee_pose = posestpd->pose;
        // ROS_INFO_STREAM_THROTTLE(0.2, "TorqueQPControl: New pose:" << endl << this->ee_pose);
    #endif
}

void TorqueController::setFextCallback(const geometry_msgs::WrenchStamped::ConstPtr& ext_wrench)
{
    this->f_ext(0,0) = ext_wrench->wrench.force.x;
    this->f_ext(1,0) = ext_wrench->wrench.force.y;
    this->f_ext(2,0) = ext_wrench->wrench.force.z;
    this->f_ext(3,0) = ext_wrench->wrench.torque.x;
    this->f_ext(4,0) = ext_wrench->wrench.torque.y;
    this->f_ext(5,0) = ext_wrench->wrench.torque.z;
}

void TorqueController::getCtrlModeCallback(const std_msgs::Bool::ConstPtr& is_q_kreg_mode)
{
    this->joint_ctrl_mode = is_q_kreg_mode->data;
    // ROS_INFO_STREAM("Control mode: " << this->joint_ctrl_mode);
}

void TorqueController::getJointStateSecCallback(const sensor_msgs::JointState::ConstPtr& joint_states)
{
    // double* data = joint_states->position.data();
    // this->q2 = Eigen::Map< Eigen::Matrix<double,7,1> >(data);
    // data = joint_states->velocity.data();
    // this->dq2 = Eigen::Map< Eigen::Matrix<double,7,1> >(data);
    for (int i = 0; i<7; i++)
    {
        this->q2[i] = joint_states->position[i];
        this->dq2[i] = joint_states->velocity[i];
    }
}

bool TorqueController::init(hardware_interface::RobotHW* robot_hardware, ros::NodeHandle& node_handle) {    
    this->node_handle = node_handle;
    auto ns = this->node_handle.getNamespace();
    auto parent_ns = ns.substr(0, ns.find('/',1)) ;
    this->parent_node_handle = ros::NodeHandle(parent_ns); // to avoid the name space torque_control
    //--------------------------------------
    // LOAD ROBOT
    //--------------------------------------
    //string arm_id;
    if (!node_handle.getParam("arm_id", this->arm_id)) {
        ROS_ERROR_STREAM("TorqueQPControl: Could not read parameter arm_id");
        return false;
    } 

    vector<string> joint_names;
    if (!node_handle.getParam("joint_names", joint_names)) {
        ROS_ERROR("Could not parse joint names");
    }

    if (joint_names.size() != 7) {
        ROS_ERROR_STREAM("Wrong number of joint names, got "
        << joint_names.size() << " instead of 7 names!");
        return false;
    }
   
    auto state_interface = robot_hardware->get<franka_hw::FrankaStateInterface>();
    if (state_interface == nullptr) {
        ROS_ERROR("Could not get state interface from hardware");
        return false;
    }
    try {
        state_handle_ = make_unique<franka_hw::FrankaStateHandle>( state_interface->getHandle(arm_id + "_robot"));
    } catch (const hardware_interface::HardwareInterfaceException& e) {
        ROS_ERROR_STREAM(
            "Exception getting state handle: " << e.what());
            return false;
    }

    auto* effort_joint_interface = robot_hardware->get<hardware_interface::EffortJointInterface>();
    if (effort_joint_interface == nullptr)
    {
        ROS_ERROR_STREAM(
            "Error getting effort joint interface from hardware");
        return false;
    }
    for (size_t i = 0; i < 7; ++i)
    {
        try 
        {
            joint_handles_.push_back(effort_joint_interface->getHandle(joint_names[i]));
        }
        catch (const hardware_interface::HardwareInterfaceException& ex)
        {
            ROS_ERROR_STREAM(
            "Exception getting joint handles: " << ex.what());
            return false;
        }
    }
    // set the servoing link for the moveit controller
    if (!node_handle.getParam("tip_link", this->control_frame_name)) {
        this->control_frame_name = "";//this->arm_id + "_joint8";
    }
    // set the endpoint translation for the servoing (for topic or tf control)
    vector<double> ee_trans(3,0.0);
    node_handle.param<vector<double>>("ee_translation", ee_trans, vector<double>(3,0.0));
    this->ee_tf << 1, 0, 0, ee_trans[0],
                   0, 1, 0, ee_trans[1],
                   0, 0, 1, ee_trans[2],
                   0, 0, 0, 1;
    ROS_INFO_STREAM("EE servoing translation offset, x: " << ee_trans[0] << ", y: " << ee_trans[1] << ", z:" << ee_trans[2]);

    // Do not use RPY representation...
    // vector<double> ee_rotat(3,0.0);
    // node_handle.param<vector<double>>("ee_rpy_rotation", ee_rotat, vector<double>(3,0.0));
    // Eigen::Matrix3d rot(Eigen::AngleAxisd(ee_rotat[2], Eigen::Vector3d::UnitZ()) * 
    //                     Eigen::AngleAxisd(ee_rotat[1], Eigen::Vector3d::UnitY()) *
    //                     Eigen::AngleAxisd(ee_rotat[0], Eigen::Vector3d::UnitX()));
    // ROS_INFO_STREAM("EE servoing rotation offset:" << endl << rot);

    // ee_tf.topLeftCorner<3, 3>() = rot;

    #if SERVOING_TYPE == TF
        // get interactive marker tf reference frame and actual frame
        node_handle.param<string>("marker_tf_reference_frame", this->reference_frame_name, "world");
        if (!node_handle.getParam("marker_tf_frame", this->marker_tf_frame_name)) {
            ROS_ERROR("TorqueQPControl: Could not read parameter marker_tf_frame");
        }
    #elif SERVOING_TYPE == TOPIC
        EE_sub = node_handle.subscribe("/equilibrium_pose", 1, &TorqueController::endeffectorPoseCallback, this);
        pose_des_publisher.init(node_handle, "topic_desired_cartesian_pose", 1);
    #endif
    f_ext_sub = node_handle.subscribe("/external_force", 1, &TorqueController::setFextCallback, this);
    ctrl_mode_sub = node_handle.subscribe("/torque_control_mode/is_joint_regularisation", 1, &TorqueController::getCtrlModeCallback, this);
    if (this->arm_id == "panda_1")
    {
        joint_state_sec_sub = node_handle.subscribe("/panda_2/franka_state_controller/joint_states", 1, &TorqueController::getJointStateSecCallback, this);
    }
    else if (this->arm_id == "panda_2")
    {
        joint_state_sec_sub = node_handle.subscribe("/panda_1/franka_state_controller/joint_states", 1, &TorqueController::getJointStateSecCallback, this);
    }

    pose_curr_publisher.init(node_handle, "current_cartesian_pose", 1);
    vel_curr_publisher.init(node_handle, "current_cartesian_velocity", 1);
    acc_curr_publisher.init(node_handle, "current_cartesian_acceleration", 1);
    adapt_gain_publisher.init(node_handle, "adapted_KP_matrix", 1);

    franka::RobotState robot_state = state_handle_->getRobotState(); 
    adapt_gains = false;
    //Get robot current state
    // Eigen::Matrix<double,7,1>  q_init(robot_state.q.data());
    // ROS_INFO_STREAM("q_init:" << endl << q_init);

    return true;
}

void TorqueController::starting(const ros::Time&)
{
    // ROS_WARN_STREAM("Starting QP Controller on the real Panda");
    // string arm_id;
    // if (!node_handle.getParam("arm_id", this->arm_id)) {
    //     ROS_ERROR_STREAM("TorqueQPControl: Could not read parameter arm_id");
    //     return;
    // }  
    // ROS_INFO_STREAM("TorqueController::starting");
    std::string robot_description;
    getRosParam("/"+arm_id+"/robot_description", robot_description);
    std::vector<std::string> joint_names;
    ros::param::get("/"+arm_id+"/torque_control/joint_names", joint_names);
    p_gains.resize(6);
    delta_p_gains.resize(6);
    delta_p_gains.setZero();
    getRosParam("/"+arm_id+"/torque_control/p_gains", p_gains);
    d_gains.resize(6);
    // getRosParam("/"+arm_id+"/torque_control/d_gains", d_gains);
    getRosParam("/"+arm_id+"/torque_control/p_gains", d_gains);
    d_gains = 2*d_gains.cwiseSqrt();
    // ROS_INFO_STREAM("d gains:  " << d_gains);
    dtorque_max.resize(7);
    getRosParam("/"+arm_id+"/torque_control/dtorque_max", dtorque_max);
    kp_reg.resize(7);
    getRosParam("/"+arm_id+"/torque_control/kp_reg", kp_reg);
    kp_reg_q.resize(7);
    getRosParam("/"+arm_id+"/torque_control/kp_reg_q", kp_reg_q);
    ki_reg_q.resize(7);
    getRosParam("/"+arm_id+"/torque_control/ki_reg_q", ki_reg_q);
    // main task gains in matrix form
    p_gains_m = Eigen::Map<Eigen::Matrix<double, 6, 6>>(p_gains.data());
    d_gains_m = Eigen::Map<Eigen::Matrix<double, 6, 6>>(d_gains.data());
    franka::RobotState robot_state = state_handle_->getRobotState(); 
    //Get robot current state
    Eigen::Matrix<double,7,1>  q_init(robot_state.q.data());
    Eigen::Matrix<double,7,1>  qd_init(robot_state.dq.data());

    // std::string namespace_str = this->node_handle.getNamespace();
    // namespace_str.erase(namespace_str.length() - 14); // panda_1/torque_control

    if (!initialized) // franka_control starts twice but the first one doesn't initialize the torque_qp ...
    {
        // dynamic reconfigure server
        server.reset(new dynamic_reconfigure::Server<TorqueQPCtrlConfig>(reconf_mutex_, ros::NodeHandle(node_handle)));
        boost::recursive_mutex::scoped_lock scoped_lock(reconf_mutex_);
        server->setCallback(boost::bind(&TorqueController::reconfigureCallback, this, _1, _2));
        scoped_lock.unlock();
        if (control_frame_name != "")
            torque_qp.setControlledFrame(control_frame_name);
        initialized = torque_qp.init(robot_description,joint_names);
        torque_qp.setRegularizationGains(kp_reg, kp_reg_q, ki_reg_q);
        torque_qp.setDTorqueMax(dtorque_max);
        model = torque_qp.getRobotModel();
        data = pinocchio::Data(model);
        #if SERVOING_TYPE == MOVE_IT
            // trajectory.interface->init(arm_id+"/robot_description",namespace_str,q_init,joint_names);
            if (control_frame_name != "")
                trajectory.interface->setControlledFrame(control_frame_name);
            trajectory.interface->init(this->parent_node_handle, "/"+arm_id+"/robot_description", q_init, joint_names);
        #elif SERVOING_TYPE == TOPIC
            // initialize pose servoing to avoid undesired behaviour while the marker is not yet defined
            pinocchio::forwardKinematics(model,data,q_init,qd_init);
            pinocchio::updateFramePlacements(model,data);
            Eigen::Affine3d initial_pose(data.oMf[model.getFrameId(torque_qp.getControlledFrame())].toHomogeneousMatrix());
            tf::poseEigenToMsg(initial_pose, this->ee_pose);
        #endif
    }
    J.resize(6,model.nv);
    J2.resize(6,model.nv);
    // f_ext = Eigen::ArrayXf::Zero(6);
    f_ext.setZero();
    joint_ctrl_mode = false;
    ROS_INFO_STREAM_ONCE("P gains init: " << std::endl << p_gains);
    ROS_INFO_STREAM_ONCE("Initial marker: " << std::endl << this->ee_pose);
}


void TorqueController::update(const ros::Time&, const ros::Duration& period) {    
    // ROS_INFO_STREAM_ONCE("TorqueController::update");
    //--------------------------------------
    // ROBOT STATE
    //--------------------------------------
    // get state variables
    franka::RobotState robot_state = state_handle_->getRobotState(); 
    //Get robot current state
    Eigen::Matrix<double,7,1>  q(robot_state.q.data());
    Eigen::Matrix<double,7,1>  qd(robot_state.dq.data());
    Eigen::Matrix<double,7,1>  tau_J_d(robot_state.tau_J_d.data());
    Eigen::MatrixXd mass_matrix_tmp;

    if (reset_target_pose)
    {
        Eigen::Matrix<double,7,1>  q_init(robot_state.q.data());
        Eigen::Matrix<double,7,1>  qd_init(robot_state.dq.data());
        pinocchio::forwardKinematics(model,data,q_init,qd_init);
        pinocchio::updateFramePlacements(model,data);
        Eigen::Affine3d initial_pose(data.oMf[model.getFrameId(torque_qp.getControlledFrame())].toHomogeneousMatrix());
        tf::poseEigenToMsg(initial_pose, this->ee_pose);
        reset_target_pose = false;
    }

    // if necessary update the model of the second robot as well
    if (adapt_gains)
    {
        // getting the apparent inertia matrix of the second robot
        pinocchio::forwardKinematics(model,data,q2,dq2);
        // ROS_INFO_STREAM_THROTTLE(1, "b");
        pinocchio::updateFramePlacements(model,data);
        // ROS_INFO_STREAM_THROTTLE(1, "c");
        pinocchio::computeJointJacobians(model,data,q2);
        // ROS_INFO_STREAM_THROTTLE(1, "d");
        pinocchio::getFrameJacobian(model, data, model.getFrameId(torque_qp.getControlledFrame()), pinocchio::ReferenceFrame::LOCAL_WORLD_ALIGNED, J2);
        pinocchio::crba(model, data, q2);
        pinocchio::computeMinverse(model,data,q2);
        data.Minv.triangularView<Eigen::StrictlyLower>() = data.Minv.transpose().triangularView<Eigen::StrictlyLower>();
        mass_matrix_tmp = data.Minv;
        lambda2 = (J2 * data.Minv * J2.transpose()).inverse();
        // ROS_INFO_STREAM_THROTTLE(3, "Minv 2 (p1): " << endl << data.Minv);
    }
    // Update model
    // First calls the forwardKinematics on the model, then computes the placement of each frame.
    pinocchio::forwardKinematics(model,data,q,qd);
    pinocchio::updateFramePlacements(model,data);
    pinocchio::computeJointJacobians(model,data,q);
    pinocchio::getFrameJacobian(model, data, model.getFrameId(torque_qp.getControlledFrame()), pinocchio::ReferenceFrame::LOCAL_WORLD_ALIGNED, J);
    pinocchio::crba(model, data, q);
    pinocchio::Motion xd = pinocchio::getFrameVelocity(model, data, model.getFrameId(torque_qp.getControlledFrame()),pinocchio::ReferenceFrame::LOCAL_WORLD_ALIGNED);

    // Update the trajectory
    // trajectory servoing with moveit, tf or topic
    #if SERVOING_TYPE == MOVE_IT
        trajectory.interface->updateTrajectory();
        pinocchio::SE3 oMdes(trajectory.interface->getCartesianPose().matrix());
        Eigen::VectorXd q_des = trajectory.interface->getJointConfiguration();
    #elif SERVOING_TYPE == TF
        try {
            marker_listener.lookupTransform(this->reference_frame_name, this->marker_tf_frame_name, ros::Time(0), marker_tf);
        }
        catch (tf::TransformException &ex) {
            ROS_ERROR_THROTTLE(1, "%s",ex.what());
        }
        Eigen::Affine3d tmp;
        tf::transformTFToEigen(marker_tf, tmp);
        pinocchio::SE3 oMdes(tmp.matrix() * ee_tf);
    #elif SERVOING_TYPE == TOPIC
        Eigen::Affine3d tmp;
        tf::poseMsgToEigen(this->ee_pose, tmp);
        pinocchio::SE3 oMdes(tmp.matrix() * ee_tf);
        // ROS_INFO_STREAM_THROTTLE(1, "TorqueQPControl: SE3 computed:" << endl << oMdes);
    #else
        ROS_ERROR_STREAM("Servoing type undefined...");
    #endif

    // Compute error
    const pinocchio::SE3 tipMdes = data.oMf[model.getFrameId(torque_qp.getControlledFrame())].actInv(oMdes);
    // Eigen::Matrix<double,6,6> adj = data.oMf[model.getFrameId(torque_qp.getControlledFrame())].toActionMatrixInverse();
    pinocchio::SE3 refpose(data.oMf[model.getFrameId(torque_qp.getControlledFrame())].rotation(), Eigen::Vector3d::Zero());
    Eigen::Matrix<double,6,6> adj = refpose.toActionMatrix();
    Eigen::Matrix<double,6,1> err = adj * pinocchio::log6(tipMdes).toVector();

    // Proportional controller with feedforward
    #if SERVOING_TYPE == MOVE_IT
        // Eigen::Matrix<double,6,1> xdd_star = p_gains.cwiseProduct(err) + (2.0 *p_gains.cwiseSqrt()).cwiseProduct( trajectory.interface->getCartesianVelocity() - xd.toVector() ) + trajectory.interface->getCartesianAcceleration();
        Eigen::Matrix<double,6,1> xdd_star = p_gains.cwiseProduct(err) + d_gains.cwiseProduct(  - xd.toVector() ) + trajectory.interface->getCartesianAcceleration();
    #elif SERVOING_TYPE == TF || SERVOING_TYPE == TOPIC
        // pinocchio::computeMinverse(model,data,q);
        // data.Minv.triangularView<Eigen::StrictlyLower>() = data.Minv.transpose().triangularView<Eigen::StrictlyLower>();
        // //
        // ROS_INFO_STREAM_THROTTLE(1, "q:" << endl << q);
        // ROS_INFO_STREAM_THROTTLE(1, "J:" << endl << J);
        // ROS_INFO_STREAM_THROTTLE(1, "iM:" << endl << data.Minv);
        // p_gains_m = p_gains * J * data.Minv * J.transpose();
        // d_gains_m = 2*p_gains_m.cwiseSqrt();d_gains
        // ROS_INFO_STREAM_THROTTLE(1, "P gain matrix" << endl << p_gains_m);
        // Eigen::Matrix<double,6,1> xdd_star = p_gains_m * err + d_gains_m * ( -xd.toVector() );
        // ROS_INFO_STREAM_THROTTLE(1, "xdd star" << endl << xdd_star);
        if (adapt_gains)
        {
            // ROS_INFO_STREAM_THROTTLE(1, "J:" << endl << J);
            // ROS_INFO_STREAM_THROTTLE(1, "J2:" << endl << J2);
            pinocchio::computeMinverse(model,data,q);
            data.Minv.triangularView<Eigen::StrictlyLower>() = data.Minv.transpose().triangularView<Eigen::StrictlyLower>();
            Eigen::MatrixXd lambda_inv = J * data.Minv * J.transpose();
            p_gains_m = p_gains.asDiagonal().toDenseMatrix() + lambda_inv * lambda2 * delta_p_gains.asDiagonal().toDenseMatrix();
            // p_gains_m.block<3,3>(3,0).setZero();
            // d_gains = 2*p_gains_m.diagonal().cwiseSqrt();
            // xdd_star = p_gains_m * err + d_gains.cwiseProduct( -xd.toVector() );
            d_gains_m = 1.0*p_gains_m.sqrt();
            xdd_star = p_gains_m * err + d_gains_m * ( -xd.toVector() );
            
            // ROS_INFO_STREAM_THROTTLE(3, "D gain matrix" << endl << d_gains_m);
            // ROS_INFO_STREAM_THROTTLE(3, "P gain diag" << endl << p_gains.asDiagonal().toDenseMatrix());
            // ROS_INFO_STREAM_THROTTLE(3, "delta P gain diag" << endl << delta_p_gains.asDiagonal().toDenseMatrix());
            // ROS_INFO_STREAM_THROTTLE(3, "calcul"  << endl << (lambda2 * delta_p_gains.asDiagonal().toDenseMatrix()));
            // ROS_INFO_STREAM_THROTTLE(3, "P gain matrix" << endl << p_gains_m);
            // ROS_INFO_STREAM_THROTTLE(3, "Mass matrix " << arm_id <<  endl << data.Minv);
            // ROS_INFO_STREAM_THROTTLE(3, "Mass matrix other" <<  endl << mass_matrix_tmp);
            // ROS_INFO_STREAM_THROTTLE(3, "Jacobian matrix " << arm_id <<  endl << J);
            // ROS_INFO_STREAM_THROTTLE(3, "Lambda other " <<  endl << lambda2);
            // ROS_INFO_STREAM_THROTTLE(3, "Lambda inv " << arm_id <<  endl << lambda_inv);
        }
        else{
            xdd_star = p_gains.cwiseProduct(err) + d_gains.cwiseProduct( -xd.toVector() );
            p_gains_m = p_gains.asDiagonal().toDenseMatrix();
            // ROS_INFO_STREAM_THROTTLE(3, "d gains " << arm_id << endl << p_gains_m.diagonal().cwiseSqrt());
            // ROS_INFO_STREAM_THROTTLE(3, "P gain vector" << endl << p_gains);
            // ROS_INFO_STREAM_THROTTLE(3, "P gain * err" << endl << p_gains.cwiseProduct(err));
            // ROS_INFO_STREAM_THROTTLE(3, "P gain * err + D gain * (-xd)" << endl << p_gains.cwiseProduct(err) + d_gains_m * ( -xd.toVector() ) );
        }
        // ROS_INFO_STREAM_THROTTLE(3, "P gain vector " << arm_id << endl << p_gains); 
        // ROS_INFO_STREAM_THROTTLE(3, "P gain matrix" << endl << p_gains_m);
        // ROS_INFO_STREAM_THROTTLE(3, "err" << endl << err);
        // ROS_INFO_STREAM_THROTTLE(3, "xdd_star " << arm_id << endl << xdd_star);
        // ROS_INFO_STREAM_THROTTLE(3, "adapt " << arm_id << endl << adapt_gains);
        
    #endif

    // xdd_star = Eigen::MatrixXd::Zero(7,1);
    Eigen::Matrix<double,7,1>  joint_command;
    if (joint_ctrl_mode) 
    {
    #if SERVOING_TYPE == MOVE_IT
        joint_command = torque_qp.update(q,qd,tau_J_d,xdd_star, q_des);
    #else
        joint_command = torque_qp.update(q,qd,tau_J_d,xdd_star); 
    #endif
    }
    else 
    {
        joint_command = torque_qp.update(q,qd,tau_J_d,xdd_star);  
    }
    joint_command = joint_command - J.transpose() * f_ext;

    for (size_t i = 0; i < 7; ++i)
    {
        joint_handles_[i].setCommand(joint_command(i));
    }      
    publishCartesianState();
    publishAdaptiveGains(p_gains_m);
}

void TorqueController::publishAdaptiveGains(Eigen::MatrixXd adaptive_gain_matrix)
{
    tf::matrixEigenToMsg(adaptive_gain_matrix, adapt_gain_msg);
    
    //Publish current robot pose
    if (adapt_gain_publisher.trylock())
    {
        adapt_gain_publisher.msg_= adapt_gain_msg;
        adapt_gain_publisher.unlockAndPublish();
    }

}

void TorqueController::publishCartesianState()
{
    Eigen::Affine3d cartesian_pose;
    cartesian_pose.matrix() = data.oMf[model.getFrameId(torque_qp.getControlledFrame())].toHomogeneousMatrix();
    tf::poseEigenToMsg(cartesian_pose,cartesian_pose_msg);
    xd = pinocchio::getFrameVelocity(model, data, model.getFrameId(torque_qp.getControlledFrame()),pinocchio::ReferenceFrame::WORLD );
    tf::twistEigenToMsg(xd.toVector(), cartesian_velocity_msg);
    tf::accelEigenToMsg(xdd_star, cartesian_acceleration_msg);
    #if SERVOING_TYPE == TOPIC
        Eigen::Affine3d cartesian_des_pose;
        tf::poseMsgToEigen(this->ee_pose, cartesian_des_pose);
        cartesian_des_pose.matrix() = cartesian_des_pose.matrix() * ee_tf;
        tf::poseEigenToMsg(cartesian_des_pose,cartesian_des_pose_msg);
        //Publish robot desired pose
        if (pose_des_publisher.trylock())
        {
            pose_des_publisher.msg_.header.stamp = ros::Time::now();
            pose_des_publisher.msg_.header.frame_id = "world"; //this->arm_id + "_link0"; //
            pose_des_publisher.msg_.pose = cartesian_des_pose_msg;
            pose_des_publisher.unlockAndPublish();
        }
    #endif
    //Publish current robot pose
    if (pose_curr_publisher.trylock())
    {
        pose_curr_publisher.msg_.header.stamp = ros::Time::now();
        pose_curr_publisher.msg_.header.frame_id = "world";
        pose_curr_publisher.msg_.pose = cartesian_pose_msg;
        pose_curr_publisher.unlockAndPublish();
    }
    //Publish robot desired velocity
    if (vel_curr_publisher.trylock())
    {
        vel_curr_publisher.msg_.header.stamp = ros::Time::now();
        vel_curr_publisher.msg_.header.frame_id = "world";
        vel_curr_publisher.msg_.twist = cartesian_velocity_msg;
        vel_curr_publisher.unlockAndPublish();
    }
    //Publish robot desired acceleration
    if (acc_curr_publisher.trylock())
    {
        acc_curr_publisher.msg_.header.stamp = ros::Time::now();
        acc_curr_publisher.msg_.header.frame_id = "world";
        acc_curr_publisher.msg_.accel = cartesian_acceleration_msg;
        acc_curr_publisher.unlockAndPublish();
    }
}
}  // namespace panda_qp_control

PLUGINLIB_EXPORT_CLASS(panda_qp_control::TorqueController,
controller_interface::ControllerBase)
