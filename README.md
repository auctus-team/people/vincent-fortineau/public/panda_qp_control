[![pipeline status](https://gitlab.inria.fr/auctus-team/components/robots/panda/panda_qp_control/badges/master/pipeline.svg)](https://gitlab.inria.fr/auctus-team/components/robots/panda/panda_qp_control)

# Panda qp control

A ROS package to control a Franka Emika panda robot using the Quadratic Programing controllers [velocity_qp](https://gitlab.inria.fr/auctus-team/components/control/velocity_qp) and [torque_qp](https://gitlab.inria.fr/auctus-team/components/control/torque_qp).

## Installation

Procedure to install this package along the other necessary ros packages inside a catkin workspace is available [here](https://gitlab.inria.fr/auctus-team/components/robots/panda/panda_qp_ws)

